Vinayak Steels Limited


Vinayak Steels Limited is an Integrated Steel Plant manufacturing high quality Fe550 TMT Bars as per BIS 1786-2008 standard in Hyderabad, Telangana.
Vinayak Thermex TMT bar are produced by innovative method of direct charging of continuous cast billets to rolling mill, which leads to the advantages of saving energy, increasing production capacity, reducing burning loss, decreasing rolling defects and better surface finish of finished product.
